<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>User Management</title>
    <base href="{{asset('').'/backend/'}}">
    <script src="js/jquery.min.js"></script>
    <link rel="stylesheet" href="css/bootstrap.min.css" />
    <script src="js/jquery.dataTables.min.js"></script>
    <script src="js/dataTables.bootstrap.min.js"></script>
    <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" />
    <link rel="stylesheet" href="css/custom.css" />
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <br />
    <h3 align="center">User Management</h3>
    <br />
    <div align="right">
        <button type="button" name="create_record" id="create_record" class="btn btn-success btn-sm" data-toggle="modal" data-target="#userModal">Add</button>
    </div>
    <br />
    <div class="table-responsive rounded">
        <table id="user_table" class="table table-bordered table-striped table-hover" style="border-radius: 10px">
            <thead>
            <tr class="bg-success">
                <th>FullName</th>
                <th>Email</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Role</th>
            </tr>
            </thead>
            <tbody>
                @foreach($users as $user)
                    <tr style="cursor: pointer">
                        <td>{{$user->fullname}}</td>
                        <td>{{$user->email}}</td>
                        <td>{{$user->address}}</td>
                        <td>{{$user->phone}}</td>
                        <td>
                            @if($user->role==1)
                                {{"admin"}}
                            @else
                                {{"staff"}}
                            @endif
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <!-- Modal -->
    <div id="userModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Add User</h4>
                </div>
                <div class="modal-body">
                    <span id="form_result"></span>
                    <form action="" method="post" id="userForm" class="form-horizontal">
                        @csrf
                        <div class="form-group">
                            <label class="control-label col-md-4" >FullName : </label>
                            <div class="col-md-8">
                                <input type="text" name="fullname" id="fullname" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" >Email : </label>
                            <div class="col-md-8">
                                <input type="email" name="email" id="email" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" >Password : </label>
                            <div class="col-md-8">
                                <input type="password" name="password" id="password" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" >Address : </label>
                            <div class="col-md-8">
                                <input type="text" name="address" id="address" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" >Phone : </label>
                            <div class="col-md-8">
                                <input type="text" name="phone" id="phone" max="10" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4" >Role : </label>
                            <div class="col-md-8">
                                <select name="role" id="role" class="form-control">
                                    <option value="1">Admin</option>
                                    <option value="2">Staff</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" align="center">
                            <input type="submit" name="action_button" id="action_button" class="btn btn-warning" value="Add" />
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>

        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(document).on('submit','#userForm',function(event){
            event.preventDefault();
            $.ajax({
                url: "{{route('user.store')}}",
                method : "post",
                data: $(this).serialize(),
                dataType:"json",
                error:function(response){
                    let html = "<div class='alert alert-danger'>";
                    for (let key in response.responseJSON.errors) {
                        html += "<li class='mt-10'>"+response.responseJSON.errors[key]+"</li>";
                    }
                    html += "</div>";
                    $("#form_result").html(html);
                    // console.log(typeof(response.responseJSON.errors));

                    // foreach(response.responseJSON.errors);
                },
                success:function(response){
                    alert(response);
                    $("#form_result").html('');
                    console.log(response);
                }
            })
        })
    })
</script>
</body>
</html>
